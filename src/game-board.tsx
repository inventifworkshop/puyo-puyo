// Import React
import * as React from 'react'
import "./game.css"
// Define props for GameBoard component
type GameBoardProps = {
  board: number[][],
  gameOver: boolean,
  score: number
}

// Create GameBoard component
const GameBoard: React.FC<GameBoardProps> = (props) => {

  return (
    <div className="GameBoard">
      {/* Game board */}
      <div className={props.gameOver ? "board game-over" : "board"}>
        {props.board[0].map((_, y) => (
          <Row key={y}>
            {props.board.map((_, x) => (
              <Cell key={`${y}-${x}`} color={props.board[x][y]} />
            ))}
          </Row>
        ))}
      </div>
      {/* Game info */}
      <div className="board-info">
        <p className="score-text">Score: {props.score}</p>
        {props.gameOver && <p className="game-over-text"><strong>Game Over</strong></p>}
      </div>
    </div>
  )
}

interface CellProps {
  color: number
}

function Cell(props: CellProps) {
  let className = "";
  switch(props.color) {
    case 1: {
      className = "puyo-red cell";
      break;
    }
    case 2: {
      className = "puyo-green cell";
      break;
    }
    case 3: {
      className = "puyo-blue cell";
      break;
    }
    case 4: {
      className = "puyo-yellow cell";
      break;
    }
    default: {
      className = "empty cell";
      break;
    }
  }
  return <div className={className}></div>
}

interface RowProps {
  children: React.ReactNode
}

function Row(props: RowProps) {
  return <div className="row">{props.children}</div>
}

export default GameBoard
