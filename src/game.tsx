// Import React
import * as React from 'react'

// Import GameBoard component
import GameBoard from './game-board'

//Import functions to handled the board
import { Puyo, UpdateBoard, MovePieceLeft, MovePieceRight, RotatePiece, BOARD_WIDTH, BOARD_HEIGHT } from './game-helper'

// Define props for Game component state
type GameState = {
  score: number,
  gameOver: boolean,
  gamePaused: boolean,
  speedUp: boolean,
  board: number[][],
  timerId: any,
  piece: Puyo[]
}

// Create Game component
class Game extends React.Component<{}, GameState> {
  constructor(props: any) {
    super(props)

    // Generate board 
    let board = Array(BOARD_WIDTH).fill(0).map(() => Array(BOARD_HEIGHT).fill(0));

    //Generate Piece
    let piece = Array<Puyo>();
    piece[0] = { x: 2, y: -1, color: Math.floor(Math.random() * 4 + 1) };
    piece[1] = { x: 3, y: -1, color: Math.floor(Math.random() * 4 + 1) };


    // Initialize state with starting conditions
    this.state = {
      score: 0,
      gameOver: false,
      gamePaused: true,
      speedUp: false,
      board: board,
      timerId: null,
      piece: piece
    }
  }


  componentDidMount() {
    document.addEventListener("keydown", this.handleKeyDown);
    document.addEventListener("keyup", this.handleKeyUp);

    let timerId;
    timerId = window.setInterval(
      () => this.handleBoardUpdate('down'), 500)

    this.setState({
      timerId: timerId
    })
  }

  /**
   * @description Resets the timer when component unmounts
   * @memberof Game
   */
  componentWillUnmount() {
    document.removeEventListener("keydown", this.handleKeyDown);
    document.removeEventListener("keyup", this.handleKeyDown);
    window.clearInterval(this.state.timerId)
  }

  /**
   * @description Handles board updates
   * @param {string} command
   * @memberof Game
   */
  handleBoardUpdate(command: string) {
    // Do nothing if game ends, or is paused
    if (this.state.gameOver || this.state.gamePaused) {
      return
    }

    let board: number[][] = this.state.board
    let piece: Puyo[] = this.state.piece
    let score: number = this.state.score;
    let gameOver: boolean = this.state.gameOver;

    //Piece falling
    if (command === "down") {
      score = UpdateBoard(board, piece, score);
    }
    else if (command === "ArrowLeft")//Move piece to left
    {
      MovePieceLeft(board, piece);
    }
    else if (command === "ArrowRight")//Move Piece to right
    {
      MovePieceRight(board, piece);
    }
    else if (command === "ArrowUp")//Rotate piece
    {
      RotatePiece(board, piece);
    }
    else if (command === "ArrowDown")//Accelerate time
    {
      if (!this.state.speedUp) {
        // Prepare new timer
        let timerId;
        clearInterval(this.state.timerId)
        timerId = window.setInterval(
          () => this.handleBoardUpdate('down'), 50)
        this.setState({
          timerId: timerId,
          speedUp: true
        })
      }
    }

    //Update board and piece
    if (piece[0].color !== 0)
      board[piece[0].x][piece[0].y] = piece[0].color;
    if (piece[1].color !== 0)
      board[piece[1].x][piece[1].y] = piece[1].color;

    //GameOver if the piece is out of bounds
    if ((piece[0].y === -1 || piece[1].y === -1) && (piece[0].x !== piece[1].x) && (board[piece[0].x][0] !== 0 || board[piece[1].x][0] !== 0)) {
      gameOver = true;
    }
    // Update state
    this.setState({
      board: board,
      piece: piece,
      score: score,
      gameOver: gameOver
    })
  }

  /**
   * @description Stops and resumes the game
   * @memberof Game
   */
  handlePauseClick = () => {
    this.setState(prev => ({
      gamePaused: !prev.gamePaused
    }))
  }

  /**
   * @description Resets the game
   * @memberof Game
   */
  handleNewGameClick = () => {
    // Create an empty board
    let board = Array(6).fill(BOARD_WIDTH).map(() => Array(BOARD_HEIGHT).fill(0));

    //Create new piece
    let piece = Array<Puyo>();
    piece[0] = { x: 2, y: -1, color: Math.floor(Math.random() * 4 + 1) };
    piece[1] = { x: 3, y: -1, color: Math.floor(Math.random() * 4 + 1) };

    // Initialize state with starting conditions
    this.setState({
      score: 0,
      gameOver: false,
      board: board,
      piece: piece
    })
  }

  handleKeyDown = (event: KeyboardEvent) => {
    this.handleBoardUpdate(event.code)
  }

  handleKeyUp = (event: KeyboardEvent) => {
    if (event.code === "ArrowDown") {
      // Prepare new timer
      let timerId;
      clearInterval(this.state.timerId)
      timerId = window.setInterval(
        () => this.handleBoardUpdate('down'), 500)

      this.setState({
        timerId: timerId,
        speedUp: false
      })
    }
  }

  render() {
    return (
      <div className="mainContainer">
        {/* Game board */}
        <GameBoard
          board={this.state.board}
          gameOver={this.state.gameOver}
          score={this.state.score}
        />

        {/* Buttons to control game */}
        <div className="game-controls">
          <button className="btn" onClick={this.handleNewGameClick}>New Game</button>

          <button className="btn" onClick={this.handlePauseClick}>{this.state.gamePaused ? 'Play' : 'Pause'}</button>
        </div>
        {/* Game info*/}
        <div className='game-info'>
          <ul>
            <li>Use <span>LEFT</span> arrow to move piece to left</li>
            <li>Use <span>RIGHT</span> arrow to move piece to right</li>
            <li>Use <span>UP</span> arrow to rotate piece</li>
            <li>Use <span>DOWN</span> arrow to increase speed falling piece</li>
          </ul>
        </div>
      </div>
    )
  }
}

export default Game
